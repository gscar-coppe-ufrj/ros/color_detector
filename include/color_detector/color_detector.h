// color_detector.h



#ifndef COLOR_DETECTOR_H
#define COLOR_DETECTOR_H



#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"



namespace color_detector {



class ColorDetector
{
protected:
    int hMin;
    int hMax;
    int sMin;
    int sMax;
    int vMin;
    int vMax;

    float ProcessImage(cv::Mat & in, cv::Mat & out);

public:
    ColorDetector();
};



}



#endif
