// color_detector_nodelet.h



#ifndef COLOR_DETECTOR_COLOR_DETECTOR_NODELET_H
#define COLOR_DETECTOR_COLOR_DETECTOR_NODELET_H



#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <image_transport/image_transport.h>
#include <std_msgs/Float32.h>
#include "color_detector.h"



namespace color_detector {



class ColorDetectorNodelet : public ColorDetector, public nodelet::Nodelet
{
    image_transport::Subscriber msgInSub;

    image_transport::Publisher msgOutPub;
    ros::Publisher msgPercentagePub;

    void TreatIn(const sensor_msgs::ImageConstPtr & image);

public:
    virtual void onInit();
};



}



#endif
