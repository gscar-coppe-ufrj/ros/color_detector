// color_detector.cpp



#include <color_detector/color_detector.h>



color_detector::ColorDetector::ColorDetector()
{
    hMin = 0;
    hMax = 179;
    sMin = 0;
    sMax = 255;
    vMin = 0;
    vMax = 255;
}


float color_detector::ColorDetector::ProcessImage(cv::Mat & in, cv::Mat & out)
{
    cv::Mat temp;
    cv::cvtColor(in, temp, cv::COLOR_BGR2HSV); //Convert the captured frame from BGR to HSV

    cv::inRange(temp, cv::Scalar(hMin, sMin, vMin), cv::Scalar(hMax, sMax, vMax), out); //Threshold the image

    //cv::dilate(out, out, getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3,3)));
    //cv::erode(out, out, getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(8,8)));
    cv::morphologyEx(out.clone(), out, cv::MORPH_CLOSE, getStructuringElement(cv::MORPH_RECT, cv::Size(20,20)));
    cv::morphologyEx(out.clone(), out, cv::MORPH_OPEN, getStructuringElement(cv::MORPH_RECT, cv::Size(20,20)));

    int count_white = 0;
    for (int y = 0; y < out.rows; y++)
    {
        for (int x = 0; x < out.cols; x++)
        {
            if (out.at<cv::Vec3b>(y,x) == cv::Vec3b(255,255,255))
                count_white++;
        }
    }

    return (float)count_white/(float)(out.rows*out.cols);
}
