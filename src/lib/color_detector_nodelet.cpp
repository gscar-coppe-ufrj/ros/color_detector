// color_detector_nodelet.cpp



#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <color_detector/color_detector_nodelet.h>



void color_detector::ColorDetectorNodelet::onInit()
{
    ros::NodeHandle privateNH = getPrivateNodeHandle();

    privateNH.getParam("h_min", hMin);
    privateNH.getParam("h_max", hMax);
    privateNH.getParam("s_min", sMin);
    privateNH.getParam("s_max", sMax);
    privateNH.getParam("v_min", vMin);
    privateNH.getParam("v_max", vMax);

    image_transport::ImageTransport it(privateNH);

    msgOutPub = it.advertise("stream", 1);
    msgPercentagePub = privateNH.advertise<std_msgs::Float32>("percentage", 1000);

    msgInSub = it.subscribe("in", 1, &ColorDetectorNodelet::TreatIn, this, image_transport::TransportHints("raw", ros::TransportHints(), privateNH));
}


void color_detector::ColorDetectorNodelet::TreatIn(const sensor_msgs::ImageConstPtr & image)
{
    cv_bridge::CvImagePtr cvPtr;
    sensor_msgs::Image msg2 = *image;

    try
    {
        //Always copy, returning a mutable CvImage
        //OpenCV expects color images to use BGR channel order.
        cvPtr = cv_bridge::toCvCopy(msg2, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
        //if there is an error during conversion, display it
        ROS_ERROR("tutorialROSOpenCV::main.cpp::cv_bridge exception: %s", e.what());
        return;
    }

    cv_bridge::CvImage outMsg;
    outMsg.encoding = sensor_msgs::image_encodings::MONO8;
    std_msgs::Float32Ptr percentage(new std_msgs::Float32);
    percentage->data = ProcessImage(cvPtr->image, outMsg.image);

    msgOutPub.publish(outMsg.toImageMsg());
    if (percentage->data > 0.8)
        msgPercentagePub.publish(percentage);
}
